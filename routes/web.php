<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('create','EmployeeController@create');
Route::post('create','EmployeeController@store');
Route::get('employee','EmployeeController@index');
Route::get('/edit/employee/{id}','EmployeeController@edit');
Route::post('/edit/employee/{id}','EmployeeController@update');
Route::delete('/delete/employee/{id}','EmployeeController@destroy');

