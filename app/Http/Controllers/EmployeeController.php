<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //
    public function create()
    {
        return view('create');

    }

    
public function store(Request $request)
{
    $employee= new \App\employee;
    $employee->name=$request->get('name');
    $employee->position=$request->get('position');
    $employee->age=$request->get('age');
    $employee->email=$request->get('email');
    $employee->save();
    return redirect('employee')->with('success', 'Data has been added');
}

public function index()
    {
        $employees=\App\employee::all();
        return view('index',compact('employees'));
    }

    
 public function edit($id)
 {
     $employee = \App\employee::find($id);
     return view('edit',compact('employee','id'));
 }

 public function update(Request $request, $id)
    {
        $employee= \App\employee::find($id);
        $employee->name=$request->get('name');
        $employee->position=$request->get('position');
        $employee->age=$request->get('age');
        $employee->email=$request->get('email');
        $employee->save();
        return redirect('employee')->with('success', 'Data has been updated');
    }

    public function destroy($id)
    {
        $employee = \App\employee::find($id);
        $employee->delete();
        return redirect('employee')->with('success','Data has been  deleted');
    }
}
